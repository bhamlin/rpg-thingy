
class Entity:
    name = 'Default'
    race = None
    clazz = None

class EntityFactory:
    def __init__(self, base_entity, stat_block, stats=None):
        self._base_entity = base_entity
        self._stat_block = stat_block
    
    def get_entity(self):
        entity = self._base_entity()
        entity.Stats = self._stat_block()
        return entity
    