
def get_mod(value):
    return int(value / 2) - 5


class StatBlock:
    __BASE_STATS = ['str', 'dex', 'con', 'int', 'wis', 'cha']

    def __init__(self, stat_names=None):
        if not stat_names:
            stat_names = self.__BASE_STATS

        stats = dict()
        for stat in stat_names:
            stats[stat] = 8
        self._stats = stats

    def __getitem__(self, stat):
        if not stat in self._stats:
            message = f"Key '{stat}' is not in {str(tuple(self._stats.keys()))}"
            raise IndexError(message)
        return self._stats[stat]

    def __setitem__(self, stat, value):
        if not stat in self._stats:
            message = f"Key '{stat}' is not in {str(tuple(self._stats.keys()))}"
            raise IndexError(message)
        if value < 0:
            raise ValueError(f"Value '{value}' is not in the range [0, -)")
        self._stats[stat] = value

    def get_mod(self, stat):
        return get_mod(self[stat])
