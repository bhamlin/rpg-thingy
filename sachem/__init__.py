
class Game:
    def __init__(self, base_entity=None, base_statblock=None):
        from .core import StatBlock
        from .entity import Entity

        self._base_entity = base_entity or Entity
        self._base_statblock = base_statblock or StatBlock

    def __enter__(self):
        from .entity import EntityFactory

        self.entity_factory = EntityFactory(
            self._base_entity, self._base_statblock)

        return self

    def __exit__(self, type, value, traceback):
        pass
